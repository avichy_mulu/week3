/*********************************
* Class: MAGSHIMIM C2			 *
* Week:                			 *
* Name:                          *
* Credits:                       *
**********************************/

#include <iostream>
#include "vector.h"

int Vector::size() const { return _size; }



int Vector::capacity() const { return _capacity; }



int Vector::resizeFactor() const { return _resizeFactor; }


bool Vector::empty() const
{ 
	if (_size)
		return false;
	return true;
}


Vector::Vector(int n)
{
	if (n < 2)
		n = 2;
	if (empty())
	{
		_elements = new int[n];
		_elements = { 0 };
	}
}


Vector::~Vector(){
	delete[] _elements;
}


void Vector::push_back(const int& val)
{
	int curr_last = size();
	if (this->_size < this->_capacity)
		this->_elements[curr_last] = val;
	else
	{
		this->_elements = expand(this->_capacity + this->_resizeFactor, this->_elements);
		this->_elements[curr_last] = val;
		this->_capacity +=this->_resizeFactor;
	}
}



int Vector::pop_back()
{
	if (empty())
	{
		std::cout << "error: pop from empty vector\n";
		return -9999;
	}
	int num = deleteElement(this->_elements,size(),size()-1);
	this->_size -= 1;
	return num;
}

int deleteElement(int arr[], int n ,int index)
{
	// reduce size of array and move all 
	// elements on space ahead
	int num = arr[index];
	n = n - 1;
	for (int j = index; j < n; j++)
		arr[j] = arr[j + 1];
	return num;
}


void Vector::reserve(int n) 
{
	int new_capacity = this->_capacity;
	while (new_capacity < n)
	{
		new_capacity += this->_resizeFactor;
	}
	this->_elements = expand(new_capacity, this->_elements);
}


int* expand(const int size,int* elements)//extends the capacity of the vector array
{
	int i=0,* array = new int[size];
	array = { 0 };
	for (i=0;i<sizeof(elements)/4;i++)
	{
		array[i] = elements[i];
	}
	delete[] elements;
	return array;
}



void Vector::resize(int n)
{
	if(n>this->capacity())
	{
		reserve(n);
	}
	this->_size = n;
}


void Vector::assign(int val)
{
	int i = 0;
	for (i=0;i<this->size();i++)
	{
		this->_elements[i] = val;
	}
}


void Vector::resize(int n, const int& val)
{
	int last_size = this->_size;//keeping the size of the vector before changing it to n
	if (n > this->capacity())
	{
		reserve(n);
	}
	this->_size = n;
	this->assign(val,last_size);
}

void Vector::assign(const int val,const int start)
{
	int i = start;
	for (i; i < this->size(); i++)
	{
		this->_elements[i] = val;
	}
}

int& Vector::operator[](int n) const
{
	if (n < this->capacity())
		return this->_elements[n];
	std::cout << "Index is out of reach!!";
	return this->_elements[0];
}